import {setStorage} from "@/utils/storageUtil"
import {fetchAPI} from "@/api/fetchApi";
const USER_KEY = 'thjku_35w'                    //just a random string

/*
* The store object handles the saving of the global state, with setting the default states
* and updates the states with methods and stores the states.
*/
export const store = {
    state: {
        category: 0,
        difficulty: '',
        numOfQuestions: 0,
        questions: [],
        answers: [],
        index: 0
    },
    //Sets the category and stores it
    setCategory(category){
        setStorage(USER_KEY, category)
        this.state.category = category
    },
    //Sets the difficulty and stores it
    setDifficulty(difficulty){
        setStorage(USER_KEY, difficulty)
        this.state.difficulty = difficulty

    },
    //Sets the number of questions and stores it
    setNumOfQuestions(numOfQuestions){
        setStorage(USER_KEY, numOfQuestions)
        this.state.numOfQuestions = numOfQuestions
    },
    //Gets the questions from the API
    getQuestions(){
        return fetchAPI(this.state.category, this.state.difficulty, this.state.numOfQuestions).then(questions => {
            this.state.questions = questions.results
        })
    },
    //Sets the index
    setIndex(index){
        this.state.index = index
    },
    //Sets the answers and stores it
    setAnswers(answers){
        setStorage(USER_KEY, answers)
        this.state.answers.push(answers)
    },
    //resets the states
    reset(){
        this.state = {
            category: 0,
            difficulty: '',
            numOfQuestions: 0,
            questions: [],
            answers: [],
            index: 0
        }
    }
}