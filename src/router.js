import VueRouter from 'vue-router'
import HomeScreen from "./components/HomeScreen.vue";
import QuestionSite from "./components/QuestionSite.vue";
import Summary from "@/components/Summary";

//declared a variable that the vue-router uses. path, components and name defined.
const routes = [
    {
        path: '/',
        name: 'homeScreen',
        component: HomeScreen,

    },
    {
        path: '/questionsite',
        name: 'questionSite',
        component: QuestionSite
    },
    {
        path: '/summary',
        name: 'summary',
        component: Summary
    }

]


const router = new VueRouter({routes})

export default router