export function fetchAPI(category, difficulty, amountOfQuestions){      //NB!! Category is an integer!!
    let url = `https://opentdb.com/api.php?amount=${amountOfQuestions}&category=${category}&difficulty=${difficulty}`
    console.log(url)
    return fetch(url)
        .then(response => response.json())
}