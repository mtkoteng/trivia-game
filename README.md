# Trivia game

## Contributors
Markus Thomassen Koteng and Jakkris Thongma

## The task
The task for this assignment was to create a trivia game with VueJS. By importing questions from https://opentdb.com/api_config.php, we created a trivia game where the user can select category, difficulty and number of questions.

## Run
https://dry-crag-06012.herokuapp.com/#/ 

## The solution
The solution consists of a home page, with a welcome message, dropdowns where the user can select category, difficulty and number of questions, and a start button. By clicking on the start button you will be routed to the question page, where one and one question with alternatives will be displayed. After answering the last question, you will be routed to the summary page, where you can see your score, and an array of the questions with your answer and the correct answer.
### Routing
Vue Router was used in this application to route between the components. The routing is handled in route.js.
### API fetching
The JavaScript API Fetch was used to fetch the data from the API. The category, difficulty and number of questions that was chosed, was sent in as arguments in the request. The response was handled as JSON data, which was used for the game. From the response, the questions, incorrect answers (alternatives), and correct answer was used.
### Globally stored data
To share data between components which are not parent/child, we had to store the data globally. We created a JavaScript file which stored an object with states, and with needed methods. The global state was handled in store.js
### Components
- HomeScreen
    - Displays the homescreen and gives the user the opportunity to choose category, difficulty and number of questions. When these are choosed, you can click start to start the game.
- QuestionSite
    - Takes in a question with its alternatives. Handles that a new question with alternatives is shown when clicking on an alternative. Routes to Summary after lasty question.
- Question
    - Takes in and displays the question.
- Answers
    - Takes in and displays the alternatives to the specific question.
- Summary
    - Shows how the quiz went, with the answer of the user and the correct answer.
